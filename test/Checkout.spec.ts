import { Checkout } from '../src/Checkout';

describe('Checkout', () => {
  it('can instantiate', () => {
    const checkout = new Checkout();
  });

  it('can add item', () => {
    const checkout = new Checkout();
    checkout.addItem('a');
  });

  it('can add price for item', () => {
    const checkout = new Checkout();
    checkout.addItemPrice('a', 200);
  });
});