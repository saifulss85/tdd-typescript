export class Checkout {
  prices: object;
  items: string[];

  constructor() {
    this.prices = {};
  }

  addItemPrice(item: string, price: number): any {
    this.prices[item] = price;
  }

  addItem(item: string): void {

  }
}